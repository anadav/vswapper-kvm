#include <linux/vswapper.h>
#include <linux/bitmap.h>
#include <linux/module.h>
#include <linux/mmu_context.h>
#include <linux/pagemap.h>
#include <linux/mm.h>
#include <linux/memcontrol.h>
#include <linux/fs.h>
#include <linux/mman.h>
#include <linux/kvm_host.h>
#include <trace/events/kvm.h>
#include <linux/file.h>
#include <linux/anon_inodes.h>

enum {  LOFFSET_RANDOM_ACCESS = -2 };

#define FAKEM_SCAN_TIME 	(1 * 0xB2D05E00ULL)
#define PFERR_WRITE_MASK (1U << 1)

enum {
	MO_CLOSED		= (1U<<0),
	MO_BACKWARD		= (1U<<1),
	MO_SCHED_GET		= (1U<<2),
	MO_READY		= (1U<<3),
};

void kvm_enable_preventer(struct kvm *kvm)
{
	/* XXX: check whether direct_map is set on x86 */
	kvm->dcache.enabled = true;
}

int kvm_is_preventer_enabled(struct kvm* kvm)
{
	return kvm->dcache.enabled;
}

void kvm_register_vm_image(struct kvm *kvm, struct file *filp)
{
	/* TODO: Support multiple VM images */
	kvm->vm_image_filp = filp;
}

int kvm_is_vm_image_filp(struct kvm *kvm, struct file *filp)
{
	return kvm->vm_image_filp != NULL && kvm->vm_image_filp == filp;
}

static inline unsigned long cpage_org_address(struct kvm_cpage *cpage)
{
	return (unsigned long)cpage->vcpu->kvm->dcache.p_realpages + 
		cpage->realpage_idx * PAGE_SIZE;
}

static struct kmem_cache *kvm_cpage_cache; 

static void free_cpage(struct kvm_cpage *cpage, int shutdown)
{
	struct kvm *kvm = cpage->vcpu->kvm;
	hva_t org_addr = cpage_org_address(cpage);
	if (likely(!shutdown))
		vm_mmap(NULL, org_addr, PAGE_SIZE, PROT_READ | PROT_WRITE, 
			MAP_ANONYMOUS|MAP_PRIVATE|MAP_FIXED, 0);

	if (cpage->newpage) {
		mem_cgroup_uncharge_page(cpage->newpage);
		put_page(cpage->newpage);
		cpage->newpage = NULL;
	}
	if (cpage->oldpage) {
		if (trylock_page(cpage->oldpage)) {
			invalidate_inode_page(cpage->oldpage);
			unlock_page(cpage->oldpage);
		}
		put_page(cpage->oldpage);
	}
	clear_bit(cpage->realpage_idx, kvm->dcache.used_realpages);
	kmem_cache_free(kvm_cpage_cache, cpage);
}

static void free_cpages(struct kvm* kvm, int shutdown)
{
	struct hlist_head to_free_cpages;
	if (hlist_empty(&kvm->dcache.free_cpages))
		return;

	spin_lock(&kvm->dcache.free_lock);
	hlist_move_list(&kvm->dcache.free_cpages,
			&to_free_cpages);
	spin_unlock(&kvm->dcache.free_lock);
	while (!hlist_empty(&to_free_cpages)) {
		struct kvm_cpage *cpage = hlist_entry(to_free_cpages.first, 
				struct kvm_cpage, link);
		hlist_del_init(&cpage->link);
		free_cpage(cpage, shutdown);
	}
}

static void prepare_cpage_removal(struct kvm_cpage *cpage)
{
	struct kvm *kvm = cpage->vcpu->kvm;
	spin_lock(&kvm->dcache.free_lock);	
	hlist_add_head(&cpage->link, &kvm->dcache.free_cpages);
	spin_unlock(&kvm->dcache.free_lock);
}

static inline int get_cpage_unless_zero(struct kvm_cpage *cpage)
{
	return atomic_inc_not_zero(&cpage->ref);
}

static void get_cpage(struct kvm_cpage *cpage)
{
	atomic_inc(&cpage->ref);
}

static void put_cpage(struct kvm_cpage *cpage)
{
	if (atomic_dec_and_test(&cpage->ref))
		prepare_cpage_removal(cpage);
}

static inline bool kvm_empty_cpages(struct kvm *kvm, hva_t hva)
{
	return hlist_empty(&kvm->dcache.cpages);
}

static struct kvm_cpage* __kvm_find_cpage(struct kvm *kvm, hva_t hva,  
			struct kvm_cpage *new_cpage)
{
	struct kvm_cpage *cpage = NULL;
	struct hlist_node *cpage_node;
	hva &= PAGE_MASK;
	spin_lock(&kvm->dcache.lock);	

	hlist_for_each(cpage_node, &kvm->dcache.cpages) {
		cpage = hlist_entry(cpage_node, struct kvm_cpage, link);
		if (cpage->hva == hva) 
			goto out;
		if (cpage->hva < hva) 
			break;
	}
	if (!new_cpage) {
		cpage = NULL;
		goto out_nocpage;
	}
	/* Creating a new cpage */
	if (cpage) {
		if (cpage_node) 
			hlist_add_before(&new_cpage->link, &cpage->link);
		else /* Last entry */
			hlist_add_after(&cpage->link, &new_cpage->link);
	} else 
		hlist_add_head(&new_cpage->link, &kvm->dcache.cpages);
	cpage = new_cpage;
out:
	if (!get_cpage_unless_zero(cpage))
		cpage = NULL;
out_nocpage:
	spin_unlock(&kvm->dcache.lock);
	return cpage;
}

static struct kvm_cpage* kvm_find_cpage(struct kvm *kvm, hva_t hva)
{
	return __kvm_find_cpage(kvm, hva, NULL);
}

int kvm_read_cpage(struct kvm_vcpu *vcpu, hva_t hva, gfn_t gfn, void *data,
		       int offset, int len)
{
	unsigned short done = 0;
	struct kvm_cpage *cpage = NULL; 
	struct kvm *kvm = vcpu->kvm;
	u8 *cpage_data = NULL;
	if (!kvm_is_preventer_enabled(kvm))
		return len;
	cpage = kvm_find_cpage(kvm, hva);
	if (cpage == NULL)  
		return len;
	spin_lock(&cpage->lock);
	if (cpage->flags & MO_CLOSED)
		goto out_up;
	for (done = 0; done < len; done++) 
		if (!test_bit(done + offset, cpage->dirty))  
			goto out_up;	/* miss */
	trace_kvm_read_cpage(hva, gfn);
	cpage_data = kmap_atomic(cpage->newpage);
	memcpy(data, &cpage_data[offset], len);
	kunmap_atomic(cpage_data);
out_up:
	spin_unlock(&cpage->lock);
	put_cpage(cpage);
	return len - done;
}

void kvm_vswapper_advice(struct kvm *kvm, hva_t hva, int write,
		bool *should_emulate, bool *no_source,
		bool *tracked)
{
	struct kvm_cpage *cpage = NULL; 
	unsigned int offset = hva & (~PAGE_MASK);
	*no_source = true;
	*should_emulate = false;
	*tracked = false;

	if (!kvm_empty_cpages(kvm, hva))
		cpage = kvm_find_cpage(kvm, hva);

	/* Check one byte is present */
	if (!cpage) {
		if (write)
			/* We do not know op-size, and anyhow
			   x86 architecture sometimes gives different
			   bytes than the first byte */
			*should_emulate = (offset <= 8 || 
				offset > PAGE_SIZE - 8);
		goto out;
	}
	/* We do it without a lock and risk in wrong results */
	if (cpage->flags & MO_CLOSED)
		goto put_out;
	*tracked = true;
	if (!write && !test_bit(offset, cpage->dirty))
		goto put_out;
	/* If we got here we can emulate */

	*no_source = false;
	if (cpage->flags & MO_SCHED_GET)
		goto put_out;

	*should_emulate = true;		
put_out:
	if (cpage)
		put_cpage(cpage);
out:	
	return;
}

static inline int kvm_need_cpage_async_io(struct kvm_cpage *cpage)
{
	u64 time;
	rdtscll(time);
	return (cpage->loffset == LOFFSET_RANDOM_ACCESS ||
		time - cpage->cr_time > FAKEM_SCAN_TIME);
}

static bool kvm_sched_cpage_if_needed(struct kvm_cpage *cpage)
{
	if (cpage->flags & MO_SCHED_GET) 
		return false;
	if (cpage->oldpage || cpage->n_bytes == PAGE_SIZE)
		goto need;
	if (kvm_need_cpage_async_io(cpage)) 
		goto need;
	return false;
need:
	cpage->flags |= MO_SCHED_GET;
	return true;
}	

static void map_cpage(struct kvm_cpage *cpage)
{
	/* XXX: A bit x86 custom stuff */
	kvm_arch_setup_async_pf_ready(cpage->vcpu, cpage->gfn << PAGE_SHIFT, 
		cpage->gfn, cpage->hva, 
		cpage->newpage, PFERR_WRITE_MASK, false, 0, true);
}

static void kvm_sched_cpage(struct kvm_cpage *cpage, bool async)
{
	int npages;
	struct page *page;
	struct kvm *kvm = cpage->vcpu->kvm;
	npages = kvm_get_user_page(current, kvm->mm, cpage->hva, 
		(async ? KVM_HTP_FLAG_ASYNC : KVM_HTP_FLAG_WRITE_FAULT), NULL, &page);
	if (npages == 1) {
		map_cpage(cpage);
		put_page(page);
		return;
	}
	kvm_arch_setup_async_pf(cpage->vcpu, cpage->gfn << PAGE_SHIFT, 
		cpage->gfn, PFERR_WRITE_MASK, false);
}

void kvm_periodic_sched_fakem(struct kvm *kvm, bool force)
{
	struct hlist_node *cpage_node;
	u64 time;

	if (!force) {	
		rdtscll(time);
		if (time - kvm->dcache.scan_time < FAKEM_SCAN_TIME ||
			!kvm_is_preventer_enabled(kvm))
			return;
		kvm->dcache.scan_time = time;
	}
	if (hlist_empty(&kvm->dcache.cpages))
		return;

	spin_lock(&kvm->dcache.lock);
restart:
	hlist_for_each(cpage_node, &kvm->dcache.cpages) {
		struct kvm_cpage *cpage = hlist_entry(cpage_node, 
			struct kvm_cpage, link);
		get_cpage(cpage);
		if (kvm_sched_cpage_if_needed(cpage)) {
			spin_unlock(&kvm->dcache.lock);
			kvm_sched_cpage(cpage, !force);
			put_cpage(cpage);
			spin_lock(&kvm->dcache.lock);
			/* Lock was dropped - need restart */
			goto restart;
		}
		put_cpage(cpage);
	}
	spin_unlock(&kvm->dcache.lock);
}
EXPORT_SYMBOL_GPL(kvm_periodic_sched_fakem);

static void kvm_track_cpage_async_io(struct kvm_cpage *cpage, 
					int offset, int len)
{
	if (cpage->loffset == LOFFSET_RANDOM_ACCESS)
		return;
	if (cpage->n_bytes == 0) {
		if (offset == 0)
			cpage->loffset = len - 1;
		else if (offset == PAGE_SIZE - len) {
			cpage->flags |= MO_BACKWARD;
			cpage->loffset = offset;
		} else
			cpage->loffset = LOFFSET_RANDOM_ACCESS;
	} else {
		if (!(cpage->flags & MO_BACKWARD) && 
			offset == cpage->loffset + 1)
			cpage->loffset += len;
		else if ((cpage->flags & MO_BACKWARD) && 
			offset + len == cpage->loffset)
			cpage->loffset -= len;
		else
			cpage->loffset = LOFFSET_RANDOM_ACCESS;
	}
}

static inline int is_normal_non_present_pte(struct kvm *kvm, hva_t hva)
{
	struct vm_area_struct *vma;
	pte_t *ptep;
	spinlock_t *ptl;
	int ret = false;
 	vma = find_vma_intersection(kvm->mm, hva, hva + 1);
	if (!vma || (vma->vm_flags & VM_SHARED))
		return false;
	ptep = get_locked_pte(kvm->mm, hva, &ptl);
	ret = ptep && !pte_present(*ptep) &&
		((!pte_none(*ptep) && !pte_file(*ptep)) /* i.e., swap */ || 
		(pte_none(*ptep) && vma->vm_ops && vma->vm_ops->fault)); 
	pte_unmap_unlock(*ptep, ptl);
	return ret;
}

/* Must hold mm_sem for write */
static struct kvm_cpage*
kvm_create_cpage(struct kvm_vcpu *vcpu, hva_t hva, gfn_t gfn)
{
	struct kvm_cpage *found_cpage, *cpage = NULL;
	struct kvm *kvm = vcpu->kvm;
	struct page *page = NULL;
	struct vm_area_struct *vma;
	hva_t hva_mmap;
	int res;
	u16 idx;

	found_cpage = kvm_find_cpage(kvm, hva);
	if (found_cpage)
		return found_cpage;

	/* Since we hold mm_sem, we can get index not and use it later */
	idx = find_first_zero_bit(kvm->dcache.used_realpages, MAX_SHADOW_PAGES);
	if (idx >= MAX_SHADOW_PAGES ||
		test_and_set_bit(idx, kvm->dcache.used_realpages))
		return NULL;
	page = alloc_page(GFP_HIGHUSER_MOVABLE); 
	if (!page)
		goto clear;
	if (unlikely(mem_cgroup_newpage_charge(page, kvm->mm, GFP_KERNEL))) { 
		put_page(page); /* OOM */
		goto clear;
	}
	cpage = kmem_cache_zalloc(kvm_cpage_cache, GFP_KERNEL);
	if (!cpage) {
		mem_cgroup_uncharge_page(page);
		put_page(page);
		goto clear;
	}
	INIT_HLIST_NODE(&cpage->link);
	spin_lock_init(&cpage->lock);
	cpage->hva = hva;
	cpage->gfn = gfn;
	cpage->vcpu = vcpu;
	cpage->newpage = page;
	atomic_set(&cpage->ref, 1);
	rdtscll(cpage->cr_time);
	cpage->realpage_idx = idx; 
	
	res = mremap_to(hva, PAGE_SIZE, cpage_org_address(cpage), PAGE_SIZE);
	BUG_ON(!res);
	hva_mmap = do_mmap_pgoff(kvm->dcache.vswapper_filp, 
		hva & PAGE_MASK, PAGE_SIZE, PROT_READ | PROT_WRITE,
		MAP_FIXED | MAP_PRIVATE, hva >> PAGE_SHIFT);
	BUG_ON(hva_mmap != hva);
	
	found_cpage = __kvm_find_cpage(kvm, hva, cpage);
	if (found_cpage != cpage)
		put_cpage(cpage);
	vma = find_vma_intersection(kvm->mm, hva, hva+1);
	BUG_ON(!vma);
	vma->vm_private_data = found_cpage;	
	return found_cpage;
clear:
	clear_bit(idx, kvm->dcache.used_realpages);
	return NULL;
}

/* This check has false negatives and positive*/
static int is_page_present(struct kvm *kvm, hva_t hva)
{
	pte_t *ptep;
	spinlock_t *ptl;
	int ret;
	ptep = get_locked_pte(kvm->mm, hva, &ptl);
	ret = pte_present(*ptep);
	pte_unmap_unlock(*ptep, ptl);
	return ret;
}

static int kvm_vswapper_page_write(struct kvm_vcpu *vcpu, hva_t hva, gfn_t gfn,
	const void *data, int offset, int len)
{
	struct kvm *kvm = vcpu->kvm;
	struct mm_struct *mm = kvm->mm;
	int npages, remaining = len;
	struct kvm_cpage *cpage = NULL;
	struct page *page = NULL;
	void *vto;

	hva_t new_hva;
	if (offset != 0 || len != PAGE_SIZE)
		goto out;

	down_write(&mm->mmap_sem);
	if (!is_normal_non_present_pte(kvm, hva)) 
		goto out_up;

	cpage = kvm_find_cpage(kvm, hva);
	if (cpage) {
		put_cpage(cpage);
		goto out_up;
	}

	new_hva = do_mmap_pgoff(NULL, hva, PAGE_SIZE, PROT_READ | PROT_WRITE, 
		MAP_ANONYMOUS | MAP_PRIVATE | MAP_FIXED, 0);
	BUG_ON(new_hva != hva);
	npages = kvm_get_user_page(current, mm, hva, 
		KVM_HTP_FLAG_WRITE_FAULT | KVM_HTP_FLAG_ASYNC | KVM_HTP_FLAG_NOSWAP |
		KVM_HTP_FLAG_NOPREFETCH, NULL, &page);
	BUG_ON(!page);
	vto = kmap_atomic(page);
	memcpy(vto, data, PAGE_SIZE);
	kunmap_atomic(vto);
	trace_kvm_vswapper_page_write(hva, gfn);
	trace_kvm_remap_cpage(hva, gfn);
	remaining = 0;
out_up:
	up_write(&mm->mmap_sem);
out:
	if (remaining == 0) {
		kvm_arch_setup_async_pf_ready(kvm->vcpus[0], gfn << PAGE_SHIFT, 
			gfn, hva, page, PFERR_WRITE_MASK, false, 0, true);
		put_page(page);
	}

	return remaining; 
}

static struct kvm_cpage *kvm_find_create_cpage(struct kvm_vcpu *vcpu, 
	hva_t hva, gfn_t gfn)
{
	struct kvm *kvm = vcpu->kvm;
	struct mm_struct *mm = kvm->mm;
	struct kvm_cpage *cpage = kvm_find_cpage(kvm, hva);
	if (!cpage) {
		down_write(&mm->mmap_sem);
		if (is_normal_non_present_pte(kvm, hva)) 
			cpage = kvm_create_cpage(vcpu, hva, gfn);
		up_write(&mm->mmap_sem);
	}
	return cpage;
}

int kvm_write_cpage(struct kvm_vcpu *vcpu, hva_t hva, gfn_t gfn, 
	const void *data, int offset, int len)
{
	int done = 0;
	struct kvm_cpage *cpage = NULL;
	bool sched_get = false;
	struct kvm *kvm = vcpu->kvm;
	u8 *cpage_data = NULL;

	if (!kvm_is_preventer_enabled(kvm) || is_page_present(kvm, hva))
		return len;
	if (kvm_vswapper_page_write(vcpu, hva, gfn, data, offset, len) == 0) 
		return 0;
	
	cpage = kvm_find_create_cpage(vcpu, hva, gfn);
	if (!cpage)
		return len;
	spin_lock(&cpage->lock);
	if (cpage->flags & MO_CLOSED) 
		goto out_unlock;

	trace_kvm_write_cpage(hva, gfn);
	kvm_track_cpage_async_io(cpage, offset, len);
	for (done = 0; done < len; done++) 
		if (!__test_and_set_bit(offset + done, cpage->dirty))
			cpage->n_bytes++;
	cpage_data = kmap_atomic(cpage->newpage);
	memcpy(&cpage_data[offset], data, len);
	kunmap_atomic(cpage_data);

	sched_get = kvm_sched_cpage_if_needed(cpage);
	if (cpage->n_bytes == PAGE_SIZE)
		trace_kvm_remap_cpage(hva, gfn);
out_unlock:
	spin_unlock(&cpage->lock);
	if (sched_get)
		kvm_sched_cpage(cpage, true);
	put_cpage(cpage);
	return len - done;		
}

void kvm_check_vswapper_completion(struct kvm_vcpu *vcpu)
{
	free_cpages(vcpu->kvm, false);
}

void kvm_vswapper_destroy_vm(struct kvm *kvm)
{
	BUG_ON(filp_close(kvm->dcache.vswapper_filp, NULL) < 0);
	free_cpages(kvm, true);
}

static int kvm_get_original_page(struct kvm *kvm, struct kvm_cpage *cpage, 
		unsigned int gup_flags)
{
	struct page *page = NULL;
	struct mm_struct *mm = kvm->mm;
	int n_pages, async_res, async = !!(gup_flags & FOLL_NOWAIT);
	n_pages = __get_user_pages(current, mm, cpage_org_address(cpage), 
			1, gup_flags, &page, NULL, async ? &async_res : NULL);
	if (n_pages == 0) 
		return (gup_flags & (FOLL_NOWAIT | FOLL_NOSWAPIN)) ? 
			VM_FAULT_RETRY : VM_FAULT_OOM;

	spin_lock(&cpage->lock);
	if (likely(cpage->oldpage == NULL)) {
		cpage->oldpage = page;
		page = NULL;
	}
	spin_unlock(&cpage->lock);
	if (page != NULL)
		put_page(page);
	return VM_FAULT_MAJOR;
}

static void kvm_merge_cpage_with_original(struct kvm_cpage *cpage)
{
	unsigned int s_offset, e_offset;
	void *vfrom, *vto;
	if (cpage->n_bytes == PAGE_SIZE)
		return;
	trace_kvm_vswapper_merge(cpage->hva, cpage->gfn);

	s_offset = find_first_zero_bit(cpage->dirty, PAGE_SIZE);
	vto = kmap_atomic(cpage->newpage);
	vfrom = kmap_atomic(cpage->oldpage);
	while (s_offset < PAGE_SIZE) {
		e_offset = find_next_bit(cpage->dirty, 
			PAGE_SIZE, s_offset+1);
		memcpy(vto+s_offset, vfrom+s_offset, e_offset - s_offset);
		s_offset = find_next_zero_bit(cpage->dirty, PAGE_SIZE, e_offset);
	}
	kunmap_atomic(vfrom);
	kunmap_atomic(vto);
}

static unsigned int fault_flags_to_gup_flags(unsigned int fault_flags)
{
	return FOLL_GET | 
		((fault_flags & FAULT_FLAG_NOPREFETCH) ? FOLL_NOPREFETCH : 0) |
		((fault_flags & FAULT_FLAG_RETRY_NOWAIT) ? FOLL_NOWAIT : 0) | 
		((fault_flags & FAULT_FLAG_NOSWAPIN) ? FOLL_NOSWAPIN : 0); 
}

/*
 * We never mark the pages as minor or major faults in order not to double
 * count faults that result from the original VMA
 */
static int vswapper_fault(struct vm_area_struct *vma, struct vm_fault *vmf)
{
	int ret;
	bool need_map = false;
	struct kvm_cpage *cpage = vma->vm_private_data;
	struct kvm *kvm = cpage->vcpu->kvm;
	unsigned int gup_flags = fault_flags_to_gup_flags(vmf->flags);
	get_cpage(cpage);
restart:
	spin_lock(&cpage->lock);
	if (cpage->flags & MO_READY) 
		goto gotten;
	/* Even if closed, we need to return the page */
	if (cpage->oldpage == NULL && cpage->n_bytes != PAGE_SIZE) {
		spin_unlock(&cpage->lock);
		ret = kvm_get_original_page(kvm, cpage, gup_flags);
		goto put_nopage;
	}

	kvm_merge_cpage_with_original(cpage);
	SetPageUptodate(cpage->newpage);
	if (!(cpage->flags & MO_READY) && !(cpage->flags & MO_SCHED_GET))
		need_map = true;
	cpage->flags |= MO_READY | MO_CLOSED | MO_SCHED_GET;
gotten:
	get_page(cpage->newpage);
	vmf->page = cpage->newpage;
	spin_unlock(&cpage->lock);
	if (need_map)
		map_cpage(cpage);
	put_cpage(cpage);
	return 0;
put_nopage:
	if (ret & VM_FAULT_OOM) {
		put_cpage(cpage);
		return ret;
	}
	if (ret & VM_FAULT_RETRY) {
		put_cpage(cpage);
		if (vmf->flags & FAULT_FLAG_ALLOW_RETRY) {
			if (!(vmf->flags & FAULT_FLAG_RETRY_NOWAIT))
				up_read(&vma->vm_mm->mmap_sem);
			return VM_FAULT_RETRY;
		} else 
			return VM_FAULT_SIGBUS;
	}
	goto restart;
}

static void vswapper_vma_open(struct vm_area_struct *vma)
{
	struct kvm *kvm = vma->vm_file->private_data;
	kvm_get_kvm(kvm);
}

static void vswapper_vma_close(struct vm_area_struct *vma)
{
	struct kvm *kvm = vma->vm_file->private_data;
	struct kvm_cpage *cpage = vma->vm_private_data;
	get_cpage(cpage);
	spin_lock(&cpage->lock);
	/* If there are deferred entries, we need to get the page */
	cpage->flags |= MO_CLOSED;

	put_cpage(cpage);	/* Will surely not free */
	spin_unlock(&cpage->lock);

	/* Remove it from the live cpages list - the only way out */
	spin_lock(&kvm->dcache.lock);	
	hlist_del_init(&cpage->link);
	spin_unlock(&kvm->dcache.lock);

	put_cpage(cpage); /* So it will get freed */
	kvm_put_kvm(kvm);
}

static const struct vm_operations_struct vswapper_vm_ops = {
	.open = vswapper_vma_open,
	.close = vswapper_vma_close,
	.fault = vswapper_fault,
};

static int vswapper_mmap(struct file *filp, struct vm_area_struct *vma)
{
	vma->vm_ops = &vswapper_vm_ops;
	vma->vm_flags |= VM_DONTCOPY | VM_DONTEXPAND;
	BUG_ON(vma->vm_flags & VM_SHARED);
	vswapper_vma_open(vma);
	return 0;
}

static struct file_operations vswapper_fops = {
	.mmap		= vswapper_mmap,
	.llseek		= noop_llseek,
};

int kvm_vswapper_create_vm(struct kvm *kvm)
{
	spin_lock_init(&kvm->dcache.lock);
	spin_lock_init(&kvm->dcache.free_lock);
	init_rwsem(&kvm->disk_sem);
	INIT_HLIST_HEAD(&kvm->dcache.cpages);
	INIT_HLIST_HEAD(&kvm->dcache.free_cpages);

	kvm->dcache.vswapper_filp = anon_inode_getfile("[vswapper]", 
				&vswapper_fops, kvm, O_RDWR);
	if (IS_ERR(kvm->dcache.vswapper_filp)) {
		printk(KERN_ERR "%s : error opening vswapper\n", __func__);
		kvm->dcache.vswapper_filp = NULL;
		return -EFAULT;
	}
	kvm->dcache.p_realpages = 
		vm_mmap(NULL, 0, PAGE_SIZE * MAX_SHADOW_PAGES, 
		PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, 0);
	bitmap_zero(kvm->dcache.used_realpages, MAX_SHADOW_PAGES);
	return 0;
}

int kvm_vswapper_init(void)
{
	kvm_cpage_cache = KMEM_CACHE(kvm_cpage, 0);
	if (!kvm_cpage_cache) 
		return -ENOMEM;
	return 0;
}

void kvm_vswapper_deinit(void)
{
	kmem_cache_destroy(kvm_cpage_cache);
	kvm_cpage_cache = NULL;
}

void handle_noreflect(struct file *file, loff_t pos, size_t len)
{
	struct vm_area_struct *vma;
	struct mm_struct *mm = get_task_mm(current);
	struct address_space *mapping = file->f_mapping;
	unsigned long s_f_page = pos >> PAGE_SHIFT;
	unsigned long l_f_page = (pos+len) >> PAGE_SHIFT;
	bool need_cow;
	unsigned long addr;
	down_read(&mm->mmap_sem);
restart:
	mutex_lock(&mapping->i_mmap_mutex);
	need_cow = false; 
	vma_interval_tree_foreach(vma, &mapping->i_mmap, s_f_page, l_f_page) {
		unsigned long s_vm_f_page, l_vm_f_page, vm_pages, f_page;
		if (vma->vm_mm != mm)
			continue;
		vm_pages = ((vma->vm_end - vma->vm_start) >> PAGE_SHIFT) - 1;
		s_vm_f_page = max(s_f_page, vma->vm_pgoff);
		l_vm_f_page = min(l_f_page, vma->vm_pgoff+vm_pages);

		for (f_page = s_vm_f_page; f_page <= l_vm_f_page; f_page++) {
			pte_t pte, *ptep;
			spinlock_t *ptl;
			addr = ((f_page - vma->vm_pgoff) << PAGE_SHIFT) +
						vma->vm_start;
			ptep = get_locked_pte(mm, addr, &ptl);
			BUG_ON(!ptep);
			pte = *ptep;
			if (pte_present(pte)) {
				struct page *page = vm_normal_page(vma, addr, 
					pte);
				BUG_ON(!page);
				need_cow = !PageAnon(page);
			} else 
				need_cow = (pte_none(pte) && vma->vm_ops && 
					vma->vm_ops->fault) || pte_file(pte);
			pte_unmap_unlock(pte, ptl);
			if (need_cow)
				goto do_cow; 	/* we must handle it without the mutex lock */
		}
	}
do_cow:
	mutex_unlock(&mapping->i_mmap_mutex);
	if (need_cow) {
		int ret = __get_user_pages(current, mm, addr, 1, 
				FOLL_TOUCH | FOLL_WRITE, NULL, NULL, NULL);
		BUG_ON(ret != 1);
		goto restart;
	}
	up_read(&mm->mmap_sem);
	mmput(mm);
}

ssize_t kvm_vswapper_map(struct kvm *kvm, struct kvm_mmap *map)
{
	struct fd f;
	struct iovec iovstack[UIO_FASTIOV], *vector, *iov = iovstack;
	ssize_t r;
	int nr_segs = map->nr_segs;
	loff_t pos = map->offset;
	r = rw_copy_check_uvector((map->write ? WRITE : READ), map->iovec, 
		map->nr_segs, ARRAY_SIZE(iovstack), iovstack, &iov);
	if (r <= 0) 
		return r;
	f = fdget(map->fd);
	if (!f.file)
		return -EFAULT;
	/* Need to unmap from the guest */
	/* Acquire a lock before all reads/writes, so we won't run into problem */ 

	if (map->write) {
		down_write(&kvm->disk_sem);
		handle_noreflect(f.file, pos, map->nbytes);
		r = vfs_writev(f.file, map->iovec, map->nr_segs, &pos);
		BUG_ON(r != map->nbytes);
	} else {
		force_page_cache_readahead(f.file->f_mapping, f.file, 
			map->offset >> PAGE_SHIFT, 
			(map->nbytes + PAGE_CACHE_SIZE - 1) / PAGE_CACHE_SIZE);
		down_read(&kvm->disk_sem);
	}

	vector = iov;
	pos = map->offset;
	while (nr_segs > 0) {
		void __user *base = vector->iov_base;
		size_t len = vector->iov_len;
		if ((((unsigned long)base | len | pos) & ~PAGE_MASK) == 0) {
			r = vm_mmap(f.file, (unsigned long)base, len, 
				PROT_READ | PROT_WRITE,
				MAP_FIXED | MAP_PRIVATE, pos);
			BUG_ON(r != (unsigned long)base);
			pos += len;
		} else if (!map->write) {
			r = vfs_read(f.file, base, len, &pos);
			BUG_ON(r != len);
		}
		vector++;
		nr_segs--;
	}
	if (map->write)
		up_write(&kvm->disk_sem);
	else if (kvm->vcpus[0]) {
		int nr_segs = map->nr_segs;
		vector = iov;
		up_read(&kvm->disk_sem);
		while (nr_segs > 0) {
			ssize_t len = vector->iov_len;
			hva_t hva = (hva_t)vector->iov_base;
			while (len > 0) {
				gfn_t gfn;
				struct page *page = NULL;
				int npages;
				down_read(&kvm->mm->mmap_sem);
				npages = __get_user_pages(current, kvm->mm, hva, 1, 
					FOLL_GET | FOLL_NOPREFETCH, &page, NULL, 0);
				up_read(&kvm->mm->mmap_sem);
				gfn = kvm_hva_to_gfn(kvm, hva);
				if (!is_error_pfn(gfn))
					kvm_arch_setup_async_pf_ready(kvm->vcpus[0], 
						gfn << PAGE_SHIFT, 
						gfn, hva, page, 0, false, 0, true);
				if (page)
					put_page(page);
				len -= PAGE_SIZE;
				hva += PAGE_SIZE;
			}	
			vector++;
			nr_segs--;
		}
	}

	if (iov != iovstack)
		kfree(iov);
	fdput(f);
	return map->nbytes;
}

