#ifndef __KVM_MEMOPT_H__
#define __KVM_MEMOPT_H__

#include <linux/kvm_host.h>

void kvm_enable_preventer(struct kvm *kvm);
int kvm_is_preventer_enabled(struct kvm *kvm);
void kvm_register_vm_image(struct kvm *kvm, struct file *filp);
void kvm_vswapper_advice(struct kvm *kvm, hva_t hva, int write,
		bool *should_emulate, bool *no_source, bool *tracked);
int kvm_vswapper_init(void);
int kvm_vswapper_create_vm(struct kvm *kvm);
void kvm_vswapper_deinit(void);
void kvm_vswapper_destroy_vm(struct kvm *kvm);
int kvm_read_cpage(struct kvm_vcpu *vcpu, hva_t hva, gfn_t gfn, void *data,
	int offset, int len);
int kvm_write_cpage(struct kvm_vcpu *vcpu, hva_t hva, gfn_t gfn, const void *data,
	int offset, int len);
void kvm_check_vswapper_completion(struct kvm_vcpu *vcpu);
void kvm_periodic_sched_fakem(struct kvm *kvm, bool force);
ssize_t kvm_vswapper_map(struct kvm *kvm, struct kvm_mmap *map);
#endif
